package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @RequestMapping("/main")
    public String getMain() {return "This page will contain some drop down menus" +
            " from where the user will be able to select the type of room he " +
            "needs(number of seats, projector, voice/video call capabilities, location, etc. ";}

    @RequestMapping("/calendar")
    public String getCalendar() {return "After the user chose his options and clicked next on the main page" +
            " he will be shown a calendar where the days with available rooms that satisfy his needs " +
            "will be highlighted";}

    @RequestMapping("/chooseRoom")
    public String getRoom() { return "After the user chooses the date that suits him the chooseRoom page will" +
            " show him the available rooms and he'll be able to select the one he wants and reserve it.";}
}
